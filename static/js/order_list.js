$(document).ready(function() {
    $("#yourButtonId").click(function() {
        $.ajax({
            url: '/path-to-your-api-endpoint',
            type: 'GET',
            success: function(data) {
                $("#ordersContainer").empty();

                data.forEach(function(order) {
                    $("#order_list").append(
                        `<div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">Order ID: ${order.order_id}</h5>
                                <p class="card-text">Symbol: ${order.symbol}</p>
                                <p class="card-text">Quantity: ${order.qty}</p>
                                <p class="card-text">Status: ${order.status}</p>
                            </div>
                        </div>`
                    );
                });
            },
            error: function(error) {
                console.error('Error fetching data: ', error);
            }
        });
    });
});
