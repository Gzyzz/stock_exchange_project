from django.views.generic import ListView

from api.models import Order


# Create your views here.
class Home(ListView):
    template_name = 'home.html'
    model = Order
