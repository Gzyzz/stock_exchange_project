$(document).ready(function() {
    $("#list_orders").click(function() {
        var account_id = '5e62581b-1a60-4f8d-a950-4efa12d15be7'
        console.log('work')
        $.ajax({
            url: `http://localhost:8000/api/list_orders/${account_id}`,
            type: 'GET',
            success: function(data) {
                $("#ordersContainer").empty();
                data.forEach(function(order) {
                    console.log(order)
                    $("#ordersContainer").append(
                        `<div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">Order ID: ${order.id}</h5>
                                <p class="card-text">Symbol: ${order.symbol}</p>
                                <p class="card-text">Quantity: ${order.qty}</p>
                                <p class="card-text">Status: ${order.status}</p>
                            </div>
                        </div>`
                    );
                });
            },
            error: function(error) {
                console.error('Error fetching data: ', error);
            }
        });
    });
});
