import base64
from rest_framework.views import APIView
from core.settings import API_SECRET, API_KEY, URL_ALPACA
from rest_framework.response import Response
from rest_framework import status
import requests
from .models import Order
from .serializers import OrderSerializer
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
# Create your views here.


class CreateOrderView(APIView):
    @swagger_auto_schema(
        request_body=OrderSerializer,
        responses={
            200: openapi.Response(description="Order created successfully"),
            400: openapi.Response(description="Invalid data")
        }
    )
    def post(self, request, account_id):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            try:
                order_data = request.data

                url = f'{URL_ALPACA}/v1/trading/accounts/{account_id}/orders'
                print(url)
                auth_encoded = base64.b64encode(f'{API_KEY}:{API_SECRET}'.encode()).decode()
                headers = {
                    'Authorization': f'Basic {auth_encoded}',
                    'Content-Type': 'application/json'
                }
                print(headers)
                response = requests.post(url, headers=headers, json=order_data)
                if response.status_code == 200:
                    response_data = response.json()
                    print(response_data)
                    order = Order(
                        order_id=response_data['id'],
                        client_order_id=response_data['client_order_id'],
                        symbol=response_data['symbol'],
                        qty=response_data['qty'],
                        side=response_data['side'],
                        order_type=response_data['order_type'],
                        asset_id=response_data['asset_id'],
                        time_in_force=response_data['time_in_force'],
                        limit_price=response_data['limit_price'],
                        stop_price=response_data['stop_price'],
                        created_at=response_data['created_at'],
                        extended_hours=response_data['extended_hours'],
                        updated_at=response_data['updated_at'],
                        status=response_data['status']
                    )
                    order.save()
                    return Response(response.json(), status=status.HTTP_200_OK)
                else:
                    return Response(response.json(), status=response.status_code)
            except Exception as e:
                return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OrderListView(APIView):
    def get(self, request, account_id):
        url = f'{URL_ALPACA}/v1/trading/accounts/{account_id}/orders'

        auth_encoded = base64.b64encode(f'{API_KEY}:{API_SECRET}'.encode()).decode()
        headers = {
            'Authorization': f'Basic {auth_encoded}',
            'Content-Type': 'application/json'
        }

        try:
            response = requests.get(url, headers=headers)

            if response.status_code == 200:
                return Response(response.json(), status=status.HTTP_200_OK)
            else:
                return Response(response.json(), status=response.status_code)

        except requests.exceptions.RequestException as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CancelOrderView(APIView):
    def delete(self, request, account_id, order_id):
        url = f'{URL_ALPACA}/v1/trading/accounts/{account_id}/orders/{order_id}'

        auth_encoded = base64.b64encode(f'{API_KEY}:{API_SECRET}'.encode()).decode()
        headers = {
            'Authorization': f'Basic {auth_encoded}',
            'Content-Type': 'application/json'
        }
        response = requests.delete(url, headers=headers)

        if response.status_code == 204:
            try:
                order = Order.objects.get(order_id=order_id)
                order.status = 'canceled'
                order.save()
                return Response({'message': 'Order cancelled successfully'}, status=status.HTTP_204_NO_CONTENT)
            except Order.DoesNotExist:
                return Response({'error': 'Order not found in database'}, status=status.HTTP_404_NOT_FOUND)
            else:
                return Response(response.json(), status=response.status_code)
