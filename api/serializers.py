from rest_framework import serializers
from .models import Order


class OrderSerializer(serializers.ModelSerializer):
    order_id = serializers.CharField(required=False)

    class Meta:
        model = Order
        fields = [
            'order_id', 'client_order_id', 'symbol', 'qty', 'filled_qty',
            'filled_avg_price', 'side', 'order_type', 'time_in_force',
            'limit_price', 'stop_price', 'status', 'created_at', 'updated_at',
            'submitted_at', 'filled_at', 'expired_at', 'canceled_at',
            'failed_at', 'replaced_at', 'asset_id', 'order_class',
            'extended_hours', 'commission'
        ]
        extra_kwargs = {
            'order_id': {'required': False},
            'order_type': {'required': False},
            'status': {'required': False},
            'created_at': {'required': False},
            'updated_at': {'required': False},
            'client_order_id': {'required': False, 'allow_null': True, 'allow_blank': True},

        }
