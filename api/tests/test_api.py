import pytest
from core.settings import API_SECRET, API_KEY, URL_ALPACA, BROKER_ID
import base64
import requests


def create_headers():
    auth_encoded = base64.b64encode(f'{API_KEY}:{API_SECRET}'.encode()).decode()
    headers = {
        'Authorization': f'Basic {auth_encoded}',
        'Content-Type': 'application/json'
    }
    return headers


def incorrect_create_headers():
    auth_encoded = base64.b64encode(f'bK76dLfFQRrN4C1A1341:{API_SECRET}'.encode()).decode()
    headers = {
        'Authorization': f'Basic {auth_encoded}',
        'Content-Type': 'application/json'
    }
    return headers


valid_order_data = {
    "symbol": "NKE",
    "qty": "1",
    "side": "buy",
    "type": "market",
    "time_in_force": "day"
}

invalid_order_data = {
    "symbol": "DIS",
    "qty": "10"
}

URL = f'{URL_ALPACA}/v1/trading/accounts/{BROKER_ID}/orders'


@pytest.fixture(scope="module")
def order():
    response = requests.post(url=URL, headers=create_headers(), json=valid_order_data)
    order_id = response.json().get('id')
    yield order_id

    cancel_url = f"{URL}/{order_id}"
    requests.delete(cancel_url, headers=create_headers())


@pytest.mark.parametrize("data, status_code", [(valid_order_data, 200)])
def test_create_order_success(data, status_code):
    response = requests.post(URL, headers=create_headers(), json=data)
    assert response.status_code == status_code


@pytest.mark.parametrize("data, status_code", [(invalid_order_data, 422)])
def test_create_order_client_error(data, status_code):
    response = requests.post(URL, headers=create_headers(), json=data)
    assert response.status_code == status_code


def test_order_list_success():
    response = requests.get(URL, headers=create_headers())
    assert response.status_code == 200


def test_order_list_server_error():
    wrong_account_id = "123"
    response = requests.get(f"{URL}/{wrong_account_id}/orders", headers=create_headers())
    assert response.status_code != 200


def test_cancel_order_success(order):
    cancel_url = f"{URL}/{order}"
    response = requests.delete(cancel_url, headers=create_headers())
    assert response.status_code == 204


def test_create_order_with_invalid_data():
    invalid_data = {
        "symbol": "AAPL",
        "qty": -1,
        "side": "buy",
        "type": "market",
        "time_in_force": "day"
    }
    response = requests.post(URL, headers=create_headers(), json=invalid_data)
    assert response.status_code == 422
    assert 'error' in response.json()


def test_authentication_required():
    response = requests.get(URL)
    assert response.status_code == 403


def test_invalid_authentication():
    response = requests.get(URL, headers=incorrect_create_headers())
    assert response.status_code == 403


def test_error_message_for_invalid_order():
    response = requests.post(URL, headers=create_headers(), json=invalid_order_data)
    assert response.status_code == 422
    assert 'error' in response.json()
    assert 'Invalid data' in response.json()['error']
