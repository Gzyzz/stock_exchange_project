import pytest
from django.core.exceptions import ValidationError
from api.models import Order
from django.utils import timezone


@pytest.fixture
def new_order():
    return Order(
        order_id="123456",
        symbol="AAPL",
        qty=10.00,
        side="buy",
        order_type="market",
        time_in_force="day",
        status="new",
        created_at=timezone.now(),
        updated_at=timezone.now()
    )


@pytest.mark.django_db
def test_valid_symbol():
    order = Order(symbol="AAPL")
    try:
        order.clean()
    except ValidationError:
        pytest.fail("ValidationError должно не возникать для действительного тикера")


@pytest.mark.django_db
def test_invalid_symbol():
    order = Order(symbol="INVALID")
    with pytest.raises(ValidationError):
        order.clean()


@pytest.mark.django_db
def test_create_order(new_order):
    new_order.save()
    assert Order.objects.count() == 1
    assert Order.objects.get().order_id == "123456"


@pytest.mark.django_db
def test_order_str(new_order):
    new_order.save()
    assert str(new_order) == "Order 123456 - new"


@pytest.mark.django_db
def test_invalid_qty():
    order = Order(symbol="AAPL", qty=-10.00)
    with pytest.raises(ValidationError):
        order.clean()


@pytest.mark.django_db
def test_update_order(new_order):
    new_order.save()
    order = Order.objects.get(order_id="123456")
    order.status = "completed"
    order.save()
    updated_order = Order.objects.get(order_id="123456")
    assert updated_order.status == "completed"


@pytest.mark.django_db
def test_delete_order(new_order):
    new_order.save()
    assert Order.objects.count() == 1
    new_order.delete()
    assert Order.objects.count() == 0
