from django.urls import path
from .views import CreateOrderView, OrderListView, CancelOrderView

urlpatterns = [
    path('create_order/<str:account_id>', CreateOrderView.as_view(), name='create_order'),
    path('list_orders/<str:account_id>', OrderListView.as_view(), name='list_order'),
    path('cancel_order/<str:account_id>/<str:order_id>', CancelOrderView.as_view(), name='cancel_order')
]
