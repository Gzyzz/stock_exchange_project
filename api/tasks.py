from celery import shared_task
import json
import requests
import base64
from core.settings import URL_ALPACA, API_KEY, API_SECRET
from datetime import datetime, timedelta
from django.db import transaction
from .models import Order
from django.http import StreamingHttpResponse


@shared_task
def update_order_status():
    print('Задача update_order_status запущена')
    current_date = datetime.now()
    date_five_days_ago = current_date - timedelta(days=5)
    formatted_date = date_five_days_ago.strftime('%Y-%m-%d')

    url = f'{URL_ALPACA}/v1/events/trades?since={formatted_date}'
    auth_encoded = base64.b64encode(f'{API_KEY}:{API_SECRET}'.encode()).decode()
    headers = {'Authorization': f'Basic {auth_encoded}', 'Content-Type': 'application/json'}
    print('Запрос выполнен!')

    def process_line(line):
        try:
            line = line.decode('utf-8')
            if line.startswith('data: '):
                line = line[6:]

            data = json.loads(line)

            order_info = data.get("order", {})
            order_id = order_info.get("id")
            status = order_info.get("status")

            return {
                "order_id": order_id,
                "status": status
            }
        except json.JSONDecodeError:
            print(f"Ошибка декодирования JSON: {line}")
            return {}

    def event_stream():
        with requests.get(url, headers=headers, stream=True) as response:
            for line in response.iter_lines():
                if line:
                    processed_data = process_line(line)
                    order_id, order_status = processed_data.get("order_id"), processed_data.get("status")
                    if order_id:
                        try:
                            with transaction.atomic():
                                order = Order.objects.get(order_id=order_id)
                                order.status = order_status
                                print(order_status)
                                order.save()
                                print('заказ найден')
                        except Order.DoesNotExist:
                            print(f"Заказ с ID {order_id} не найден.")
                        except Exception as e:
                            print(f"Ошибка при обновлении заказа: {e}")

    return StreamingHttpResponse(event_stream(), content_type='text/event-stream')
