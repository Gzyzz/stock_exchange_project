from django.db import models


class Order(models.Model):
    order_id = models.CharField(max_length=100, unique=True)
    client_order_id = models.CharField(max_length=100, unique=True, null=True, blank=True)
    symbol = models.CharField(max_length=10)
    qty = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    filled_qty = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    filled_avg_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    side = models.CharField(max_length=4)
    order_type = models.CharField(max_length=10)
    time_in_force = models.CharField(max_length=10)
    limit_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    stop_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    status = models.CharField(max_length=20)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    submitted_at = models.DateTimeField(null=True, blank=True)
    filled_at = models.DateTimeField(null=True, blank=True)
    expired_at = models.DateTimeField(null=True, blank=True)
    canceled_at = models.DateTimeField(null=True, blank=True)
    failed_at = models.DateTimeField(null=True, blank=True)
    replaced_at = models.DateTimeField(null=True, blank=True)
    asset_id = models.CharField(max_length=100, null=True, blank=True)
    order_class = models.CharField(max_length=20, null=True, blank=True)
    extended_hours = models.BooleanField(default=False)
    commission = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'Order {self.order_id} - {self.status}'
